# Eighty-Eighty

An emulator project for the 8080 processor written in C++

The following website is the inspiration for this project: http://emulator101.com/

Other sources referenced:
1. https://pastraiser.com/cpu/i8080/i8080_opcodes.html
2. https://altairclone.com/downloads/manuals/8080%20Programmers%20Manual.pdf
